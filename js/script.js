$(document).ready(function() {

var sections = $('sections')
  , nav = $('nav')
  , nav_height = nav.outerHeight();

var sections = $('section')
  , nav = $('nav')
  , nav_height = nav.outerHeight();

$(window).on('scroll', function () {
  var current_pos = $(this).scrollTop();
  
  sections.each(function() {
    var top = $(this).offset().top - nav_height,
        bottom = top + $(this).outerHeight();
    
    if (current_pos >= top && current_pos <= bottom) {
      nav.find('a').removeClass('active');
      sections.removeClass('active');
      
      $(this).addClass('active');
      nav.find('a[href="#'+$(this).attr('id')+'"]').addClass('active');
    }
  });
});

nav.find('a').on('click', function () {
  var $th = $(this)
    , id = $th.attr('href');
  
  $('html, body').animate({
    scrollTop: $(id).offset().top - nav_height
  }, 500);
  
  return false;
});

$(document).on("scroll",function(){
	if($(document).scrollTop()>100){ 
		$('nav').removeClass('large').addClass('small');
		}
	else{
		$('nav').removeClass('small').addClass('large');
		}
});

$('#carousel_ul li:first').before($('#carousel_ul li:last'));
$('#right_scroll img').click(function(){
    var item_width = $('#carousel_ul li').outerWidth(true);
    var left_indent = parseInt($('#carousel_ul').css('left')) - item_width;
    $('#carousel_ul:not(:animated)').animate({'left' : left_indent},500,function(){    
        $('#carousel_ul li:last').after($('#carousel_ul li:first')); 
        $('#carousel_ul').css({'left' : '-903px'});
    }); 
});

$('#left_scroll img').click(function(){
    var item_width = $('#carousel_ul li').outerWidth(true);
    var left_indent = parseInt($('#carousel_ul').css('left')) + item_width;
    $('#carousel_ul:not(:animated)').animate({'left' : left_indent},500,function(){               
        $('#carousel_ul li:first').before($('#carousel_ul li:last')); 
        $('#carousel_ul').css({'left' : '-903px'});
    });   
});

});